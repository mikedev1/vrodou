<?php

namespace App\Controller;

use App\Repository\BlogpostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogpostController extends AbstractController
{
    #[Route('/blogpost', name: 'app_blogpost')]
    public function index(BlogpostRepository $blogpostRepository): Response
    {
        $blogposts = $blogpostRepository->findAll();
        return $this->render('blogpost/index.html.twig', [
            'blogposts' => $blogposts,
        ]);
    }
}
