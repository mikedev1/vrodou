# Maison Philo

## Production environment

PHP 7.4.9

## Install node and npm in production && Symfony CLI

### Node

<https://medium.com/@iam_vinojan/how-to-install-node-js-and-npm-using-node-version-manager-nvm-143165b16ce1>

### Symfony CLI

<https://symfony.com/download>

### Fontawesome

yarn add @fortawesome/fontawesome-free


## Ecouteur d evenements

### Loggin

<https://youtu.be/UKMwwU_5wJQ>

## Initialisation

`symfony server:run`
Wampserver Mysql 5.7.31
Symfony 5.4.7

## Coding standard

### Php fixer

`phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src`
`phpcbf -v --standard=PSR12 --ignore=./src/Kernel.php ./src`

### Eslint js

`npx eslint assets`

### eslint scss

`npx stylelint "assets/**/*.scss"`

add --fix for fixes errors

## Template

### Bootstrap made

`https://bootstrapmade.com/demo/Mamba/`

## Codepen

### Speed Dial

`https://codepen.io/daviglenn/pen/ZGdLbY`

### Float button

<https://codepen.io/alvarotrigo/pen/OJmrqVB>

<https://codepen.io/fosmont/pen/oNbOQWd>

### Faker

## Chargement de fausses données

`php bin/console doctrine:fixtures:load`

### Seo

<https://www.redacteur.com/blog/seo-balise-alt-images>

### Bundle

## Imagine filter

`php bin/console liip:imagine:cache:remove`

#### Resolve

`symfony console liip:imagine:cache:resolve /images/heart.jpg`
